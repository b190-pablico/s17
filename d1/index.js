console.log("Hello World");
/*
	FUNCTIONS:
		functions in javascript are lines/blocks of codes that tell our device/application to perform certain tasks when called/invoked.

		functions are mostly created to create complicated tasks to run several lines of code in succession.

		they are used to prevent repeating lines/blocks of codes that perform the same task/function

*/

// function declaration
/*
	function keyword - defines a function in javascripts; indicator that we are creating a function that will be invoked later in our codes
	printName() - functionName; functions are named to be able to use later in the code
	function block {} - the statements inside the curly brace which comprise of the body of the function. this is where the codes are to be executed

	default syntax:
		function functionName(){
		function statements/code block;
		};
*/
function printName(){
	console.log("My name is John");
};

// call/invoke the function
/*
	the code block and statements inside a function is not immediately executed when the function is defined/declared. the code block/statements inside a function is executed when the function is invoked/called.

	it is common to use the term "call a function" instead of "invoke a function"
*/
printName();


// DECLARATION vs EXPRESSION
// FUNCTION DECLARATION - using function keyword and function name
// results in an error due to non-existence of the function(the function is not declared)
declaredFunction();

/*
	functions in javascript can be used before it can de defined, thus hoisting is allowed for the javascript functions
		NOTE: hoisting is javascript's default behavior for certain variable and functions to run/use them before their declaration
*/

function declaredFunction(){
	console.log("Hello from declaredFunction");
};

// FUNCTION EXPRESSION
/*
	a function can also be created by storing it inside a variable

	does not allow hoisting

	a function expression is an anonymouse function that is assigned to the variableFunction
		anonymouse function - unanmed function
*/
// variableFunction(); - calling a function that is created through an expression before it can be defined will result in an error ince technically it is a variable

let variableFunction = function(){
	console.log("Hello again!");
};

variableFunction();

// mini-activity


function top3Anime(){
	console.log("Top 3 Anime Recommendations: One Piece, Gintama, Sket Dance");
};

top3Anime();

function top3Movies(){
	console.log("Top 3 Movie Recommendations: Avengers: End Game, John Wick, Kingsman: The Secret Service");
};

top3Movies();

// reaggigning declared functions

declaredFunction = function(){
	console.log("updated declaredFunction");
};

declaredFunction();


// However, we cannot change the declared functions/function expressions that are defined using const
const constFunction = function(){
	console.log("Initialized const function");
};

constFunction();

/*constFunction = function(){
	console.log("cannot be re-assigned");
};

constFunction();*/

// Function scoping
/*
	scope is the accessibility of variables/functions

	Javascript variables/functions have 3 scopes
		1. local/block scope - can be accessed inside the curly brace/code block {}
		2. global scope - outer most part of the codes (it does not belong to any code block and is outside of all curly braces); can be accessed inside any curly brace/code block
		3. 
*/

{
	let localVar = "Armando Perez";
	console.log(localVar);
}

let globalVar = "Mr. Worldwide";

console.log(globalVar);
// console.log(localVar);

/*
	function scope
		JS has also function scope: each function creates a new scope
		variables defined inside a function can only be accessed and used inside that function
		variables declared with var, let, and const are quite similar which 
*/

function showNames(){
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();
// console.log(funtionVar);
// console.log(functionConst);
// console.log(functionLet);
/*
	functionVar, functionConstm and functionLet are function-scoped and cannot be accessed outside the function they are declared
*/

// Nested Functions
/*
	function that are defined inside another function. these nested functions have function scope where they can only be accessed inside the function whey they are declared/defined.
*/
function newFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);// valid since we are accessing it inside a nested function that is still inside the funtion where "name" is defined
		console.log(nestedName);
	};
	// console.log(nestedName); - returns and error since the nestedName has funtion scoping; it can only be accessed inside the nestedFuntion
nestedFunction();
}
newFunction();
// nestedFunction(); - returns an error since we must call the nestedFUntion inside the function where it is declared

// mini-acivity


let console1 = "Playstation 5";

function videoGames(){
	let console2 = "XBox S";

	console.log(console1);
	console.log(console2);
}

videoGames();

// Using alert()
	// alert() - allows us to show a small window at the top of our browser page to show information to our users, as opposed to console.log() which only shows the message on the console. it allows us to show a short dialog or instructions to our user. The page will wait (continue to load) until the user dismisses the dialog
//alert("Hello World");//will show as the page loads

// we can use an alert() to show a message to the user from a later function invocation
/*function showSampleAlert(){
	alert("Hello Again!");
};

showSampleAlert();*/
// we will find that the page waits for the user to dismiss the dialog box before proceeding. we can witness this by reloading the page while the console is open
console.log("I will be displayed after the alert has been closed.");

/*
	NOTES ON USING alert()
		show only an alert() for short dialogs/messages to the user.
		do not overuse alert() because the program/js has to wait for it to be dismissed before continuing
*/


// using prompt()
	// using promp() - used to allow us to show a small window at the top of the browser to gather user input. Much like alert(), it will have the page wait until the user completes or enters the input. The input from the prompt() will be returned as a string data type once the user dismissed the window.
	/*
		prompt("<dialoginString>");
	*/

/*let samplePrompt = prompt("Enter your name.");

console.log(typeof samplePrompt);
console.log("Hello " + samplePrompt);*/


//let nullPrompt = prompt("do not enter anything here");

//console.log(nullPrompt);//prompt() returns an empty string if the user clicks OK button without entering any informatiotion and null for users who click the CANCEL button in the window
/*
	NOTES ON USING prompt()
		it can be used to gather user input and be used in our code. however, since it will have the page wait until the user finished or closed the windows, it must not be overused.
*/


// miniactivity

/*function welcomeMessage(){

let nameFirst = prompt("Enter your first name.");
let nameLast = prompt("Enter your last name.");
console.log("Hello " + nameFirst +" "+ nameLast);
};

welcomeMessage();*/

// Function Namin

function getCourses(){
	let courses = ["science 101", "Math 101", "English 101"];
	console.log(courses);
};
getCourses();

function get(){
	let name = "Christopher";
	console.log(name);
};
get();

function foo(){
	console.log(25%5);
};
foo();





